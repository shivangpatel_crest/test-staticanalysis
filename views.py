from app import app
from app import request
from flask import render_template
from flask import url_for
from qpylib import qpylib
import utils
import json
from trustar_client import TrustarClient
from qradar_client import QRadarAPIClient
from flask import Flask, abort, session
import base64
import datetime
from trustar_auto_submit_offenses import SubmitOffensesHandler
import random
import string
import os
import sys
from flask_wtf.csrf import CSRFProtect

if getattr(sys, 'frozen', False):
    rootFolder = os.path.dirname(os.path.abspath(sys.executable))
else:
    rootFolder = os.path.dirname(os.path.abspath(__file__))

app.config['UPLOAD_FOLDER'] = os.path.abspath(os.path.join(rootFolder, '..', 'store'))

app_secret_key_file = os.path.abspath(os.path.join(rootFolder, "sec_key.txt"))
app_secret_key_exists = False
if os.path.exists(app_secret_key_file):
    with open(app_secret_key_file, 'r') as stored_key_file:
        stored_key = stored_key_file.readline().strip()
        if len(stored_key) != 0:
            app.secret_key = stored_key
            app_secret_key_exists = True
            qpylib.log('App started using a saved secret key.')
        else:
            app_secret_key_exists = False
            qpylib.log('Secret key not found, creating one...')
if not app_secret_key_exists:
    app.secret_key = os.urandom(24)
    with open(app_secret_key_file, 'wb') as new_key:
        new_key.write(app.secret_key)
        qpylib.log('New secret key has been stored')


# CSRF configuration
csrf = CSRFProtect(app)
csrf.init_app(app)


# Every relevant request will be checked for the CSRF header
@app.before_request
def check_csrf():
    csrf.protect()


@app.route('/')
@app.route('/config', methods=['GET', 'POST'])
def index():
    """
    This function handles index page flask routine.
    it stores configuration in config file and render on html page
    :return: renders index html
    """
    error_message = []
    if request.method == 'POST':
        settings_new = {
            'trustar_endpoint': request.form.get('trustar_endpoint', ''),
            'access_key': request.form.get('access_key', ''),
            'secret_key': request.form.get('secret_key', ''),
            'enclave_ids': request.form.get('enclave_ids', ''),
            'pull_enclave_ids': request.form.get('pull_enclave_ids', ''),
            'auto_submission': request.form.get('auto_submission', 'false'),
            'offense_poll_interval_type': request.form.get('offense_poll_interval_type', 'min'),
            'offense_poll_interval': request.form.get('offense_poll_interval', 5),
            'events_count_per_offense': request.form.get('events_count_per_offense', 100),
            'offense_per_iteration': request.form.get('offense_per_iteration', 500),
            'authorized_token': request.form.get('authorized_token'),
            'ioc_feed': request.form.get('ioc_feed', 'false'),
            'ioc_poll_interval_type': request.form.get('ioc_poll_interval_type', 'min'),
            'ioc_poll_interval': request.form.get('ioc_poll_interval', 5),

        }
        try:
            str_enclave_ids = settings_new['enclave_ids']
            str_enclave_ids = str_enclave_ids.replace(" ", "")
            enclave_ids = str_enclave_ids.split(',')
            str_pull_enclave_ids = settings_new['pull_enclave_ids']
            str_pull_enclave_ids = str_pull_enclave_ids.replace(" ", "")
            pull_enclave_ids = str_pull_enclave_ids.split(',')
            settings_new['enclave_ids'] = enclave_ids
            settings_new['pull_enclave_ids'] = pull_enclave_ids
            settings_new['offense_poll_interval'] = int(settings_new['offense_poll_interval'])
            settings_new['events_count_per_offense'] = int(settings_new['events_count_per_offense'])
            settings_new['offense_per_iteration'] = int(settings_new['offense_per_iteration'])
            settings_new['ioc_poll_interval'] = int(settings_new['ioc_poll_interval'])
        except Exception as e:
            error_message.append("Fail: Error while saving configuration")
            return render_template('settings.html', configurations=settings_new,
                                   error=error_message)

        is_error, err_msg = utils.validate_settings(settings_new)
        if not is_error:
            trustar_client = TrustarClient(settings_new['access_key'],
                                           utils.decrypt_password(settings_new['secret_key']),
                                           settings_new['trustar_endpoint'],
                                           False)
            # generate trustar token
            status_code = trustar_client.set_auth_token()
            if status_code is None or status_code not in [401, 200]:
                if settings_new.get('enclave_ids'):
                    settings_new['enclave_ids'] = ','.join(settings_new.get('enclave_ids'))

                if settings_new.get('pull_enclave_ids'):
                    settings_new['pull_enclave_ids'] = ','.join(settings_new.get('pull_enclave_ids'))

                error_message.append("Fail: Unable to validate TruSTAR credentials. Status Code - " + str(status_code))
                return render_template('settings.html', configurations=settings_new,
                                       error=error_message)
            if status_code == 401:
                if settings_new.get('enclave_ids'):
                    settings_new['enclave_ids'] = ','.join(settings_new.get('enclave_ids'))
                if settings_new.get('pull_enclave_ids'):
                    settings_new['pull_enclave_ids'] = ','.join(settings_new.get('pull_enclave_ids'))
                error_message.append("Fail: API credentials are Invalid. Please check your credentials")
                return render_template('settings.html', configurations=settings_new,
                                       error=error_message)

            # check Qradar Authorized Token
            qradar_client = QRadarAPIClient()
            response_qradar = qradar_client.health_check(settings_new.get('authorized_token'))

            if response_qradar.status_code == 401:
                if settings_new.get('enclave_ids'):
                    settings_new['enclave_ids'] = ','.join(settings_new.get('enclave_ids'))
                if settings_new.get('pull_enclave_ids'):
                    settings_new['pull_enclave_ids'] = ','.join(settings_new.get('pull_enclave_ids'))
                error_message.append(
                    "Fail: Authorized Service Token is invalid. Please check your Authorized Service Token.")
                return render_template('settings.html', configurations=settings_new,
                                       error=error_message)

            # store configuration in file
            status, msg = utils.store_config(settings_new)
            if status:
                error_message.append(msg)
                if settings_new.get('enclave_ids'):
                    settings_new['enclave_ids'] = ','.join(settings_new['enclave_ids'])
                if settings_new.get('pull_enclave_ids'):
                    settings_new['pull_enclave_ids'] = ','.join(settings_new.get('pull_enclave_ids'))
                return render_template('settings.html', configurations=settings_new,
                                       error=error_message)
            else:
                qpylib.log("Failed to store trustar configuration", "error")
                error_message.append("Failed to store TruSTAR configuration")
                if settings_new.get('enclave_ids'):
                    settings_new['enclave_ids'] = ','.join(settings_new['enclave_ids'])
                if settings_new.get('pull_enclave_ids'):
                    settings_new['pull_enclave_ids'] = ','.join(settings_new.get('pull_enclave_ids'))
                return render_template('settings.html', configurations=settings_new,
                                       error=error_message)
        else:
            error_message.append(err_msg)
            if settings_new.get('enclave_ids'):
                settings_new['enclave_ids'] = ','.join(settings_new['enclave_ids'])
            if settings_new.get('pull_enclave_ids'):
                settings_new['pull_enclave_ids'] = ','.join(settings_new.get('pull_enclave_ids'))
            return render_template('settings.html', configurations=settings_new,
                                   error=error_message)
    configurations = utils.read_config()
    if configurations:
        if configurations.get('enclave_ids'):
            configurations['enclave_ids'] = ','.join(configurations.get('enclave_ids'))
        if configurations.get('pull_enclave_ids'):
            configurations['pull_enclave_ids'] = ','.join(configurations.get('pull_enclave_ids'))
            return render_template('settings.html', configurations=configurations, error=error_message)

    return render_template('settings.html', configurations=configurations, error=error_message)


@app.route('/offense_list_function', methods=['GET'])
def offense_list_function():
    """
    This function is used to give offense context to GUI action javascript
    :return: offense id
    """
    offense_id = request.args.get("context")
    qpylib.log("offense_list_function: Offense id:" + offense_id, "info")
    return json.dumps({'offense_id': offense_id})


@app.route('/event_detail', methods=['GET'])
def event_detail():
    """
    This function is used to get event context to GUI action javascript
    :return: appid and event context
    """
    context = request.args.get('context')
    return_value = json.dumps({
        'appId': qpylib.get_app_id(),
        'context': context
    })
    return return_value


@app.route('/submit_event', methods=['GET'])
def submit_event():
    """
    This function is used to submit event as TruSTAR report
    :return: result as JSON
    """
    # get event context details
    context = request.args.get("context", None)
    event = request.args.get("event", "No event Context arg")
    event_name = request.args.get("event_name", "")
    low_level_category = request.args.get("low_level_category", "")
    event_desc = request.args.get("event_desc", "")
    source_ip = request.args.get("source_ip", "")
    destination_ip = request.args.get("destination_ip", "")
    start_time = request.args.get("start_time", "")

    # extract source ip and destination ip from string
    source_ip_index = source_ip.rfind("dynamicPopupMenu")
    source_ip = source_ip[0:source_ip_index]
    destination_ip_index = destination_ip.rfind("dynamicPopupMenu")
    destination_ip = destination_ip[0:destination_ip_index]
    epoch_time = datetime.datetime.strptime(start_time, "%b %d, %Y, %I:%M:%S %p").strftime("%s")
    epoch_time = int(epoch_time) * 1000
    time_begin = utils.get_ios_time(epoch_time)
    qpylib.log("time_begin:" + time_begin, "info")

    result = {
        'error': False,
        'context': 'TruSTAR submit event success'
    }
    app_id = qpylib.get_app_id()
    qpylib.log("event_name:" + event_name, "Info")
    qpylib.log("low_level_category:" + low_level_category, "Info")
    qpylib.log("event_desc:" + event_desc, "Info")
    qpylib.log("source_ip:" + source_ip, "Info")
    qpylib.log("destination_ip:" + destination_ip, "Info")

    # get trustar configuration
    configurations = utils.read_config()
    if not configurations:
        qpylib.log("configuration not found", "info")
        result['error'] = True
        result['context'] = "Submit Failed. Please setup TruSTAR configuration"
        return json.dumps(result)

    # generate report body and report id
    report_id = base64.b64encode(event_name)
    report_body = "Source IP: " + source_ip + "\n"
    report_body += "Destination IP: " + destination_ip + "\n"
    report_body += "Description: " + event_desc + "\n"
    report_body += "Raw Event: " + event

    submit_report_payload = {
        'incidentReport': {
            'title': event_name,
            'reportBody': report_body,
            'externalTrackingId': report_id,
            'distributionType': "ENCLAVE",
            'timeBegin': time_begin,
        },
        'enclaveIds': configurations['enclave_ids']
    }

    trustar_client = TrustarClient(configurations['access_key'], configurations['secret_key'],
                                   configurations['trustar_endpoint'], False)
    response_body = None

    # generate trustar auth token
    status_code = trustar_client.set_auth_token()
    if status_code == 200:
        # submit trustar report
        qpylib.log("TruSTAR set auth token success", "Info")
        if trustar_client.get_report_details(report_id):
            response_body = trustar_client.update_report(submit_report_payload)
        else:
            response_body = trustar_client.submit_report(submit_report_payload)
        if response_body is None:
            qpylib.log("Submit Failed. TruSTAR Submit report API execution failed."
                       "For more details, check QradarClient.log or TrustarClient.log files", "error")
            result['error'] = True
            result['context'] = "Submit Failed. TruSTAR Submit report API execution failed"
            return json.dumps(result)

        # add enclave tags
        report_id = response_body.get('reportId')
        if report_id:
            enclage_ids = configurations.get('enclave_ids')
            tag = low_level_category
            for enclave_id in enclage_ids:
                qpylib.log("Add Enclave tag:" + str(tag), "info")
                if not trustar_client.add_enclave_tag(report_id, tag, enclave_id):
                    qpylib.log("Add Enclave Tag failed for report id:" + str(report_id) + " tag: " + str(tag),
                               "error")
                    # end for tags
        qpylib.log("*** Event submit success ", "Info")
        return json.dumps(result)

    else:
        qpylib.log("TruSTAR authentication token failed", "error")
        result['error'] = True
        result['context'] = "TruSTAR authentication token failed"
        return json.dumps(result)


@app.route('/submit_offense', methods=['GET'])
def submit_offense():
    """
    #This function is used to submit offense as TruSTAR report
    #:return: result as JSON
    """

    offense_id = request.args.get("context", None)
    qpylib.log("submit_offense: Offense id:" + offense_id, "info")
    # get trustar configuration
    configurations = utils.read_config()
    if not configurations:
        result = {'status': False, 'message': "Submit Failed. Please setup TruSTAR configuration"}
        return json.dumps(result)

    submit_offense_handler = SubmitOffensesHandler(configurations)
    response_dict = submit_offense_handler.execute_manual(offense_id)
    return json.dumps(response_dict)


@app.route('/hunt_action', methods=['GET'])
def hunt_action():
    """
    This funtion performs hunt IP address action on Trustar
    :return: render template
    """
    # get IP address
    ip_address = request.args.get("ip")
    qpylib.log("ip address" + str(ip_address), "info")

    # get trustar configuration
    conf = utils.read_config()
    status = ""
    urls = dict()

    if not conf:
        status = "Please configure TruSTAR App Settings"
        return render_template('hunt_ip.html', trustar_indicators=urls, trustar_length=len(urls), trustar_status=status)

    client = conf['access_key']
    secret = conf['secret_key']
    trustar_host = conf['trustar_endpoint']

    # generate trustar authentication token
    trustar_client = TrustarClient(client, secret, trustar_host, False)
    status_code = trustar_client.set_auth_token()
    if status_code is None or status_code not in [401, 200]:
        status = "Unable to validate TruSTAR credentials. Status Code -" + str(status_code)
        return render_template('hunt_ip.html', trustar_indicators=urls, trustar_length=len(urls), trustar_status=status)
    if status_code == 401:
        status = "TruSTAR generate authentication failed"
        return render_template('hunt_ip.html', trustar_indicators=urls, trustar_length=len(urls), trustar_status=status)

    # get correlated indicators
    url_link = trustar_host + "/constellation/reports/"
    trustar_indicators = trustar_client.get_correlated_indicators(ip_address)
    if trustar_indicators:
        key = 'indicators'
        if key in trustar_indicators:
            trustar_indicators_list = trustar_indicators.get('indicators')
            if not trustar_indicators_list:
                qpylib.log("hunt_action: Correlated Indicators not found", "info")
                status = "TruSTAR Correlated indicators not found"
                return render_template('hunt_ip.html', trustar_indicators=urls, trustar_length=len(urls),
                                       trustar_status=status)

            # generate deep link for each ioc
            for key, values in trustar_indicators_list.items():
                for val in values:
                    url_link_part = base64.b64encode(key + "|" + str(val))
                    urls[str(val)] = url_link + url_link_part

        else:
            qpylib.log("hunt_action: Key  " + key + " not found", "info")
            status = "TruSTAR Correlated indicators not found"
            return render_template('hunt_ip.html', trustar_indicators=urls, trustar_length=len(urls),
                                   trustar_status=status)
    else:
        qpylib.log("hunt_action: No information available for IP address", "info")
        status = "TruSTAR Correlated indicators not found"
        return render_template('hunt_ip.html', trustar_indicators=urls, trustar_length=len(urls), trustar_status=status)

    return render_template('hunt_ip.html', trustar_indicators=urls, trustar_length=len(urls), trustar_status=status)


@app.route('/get_ip', methods=['GET'])
def get_ip():
    """
    This function is used to give offense context to GUI action javascript
    :return: offense id
    """
    ip = request.args.get("context")
    qpylib.log("get ip function: ip:" + ip, "info")
    return json.dumps({'ip': ip})
